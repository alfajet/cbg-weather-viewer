from django.core.management.base import BaseCommand
from cbg_weather_viewer.viewer.models import Day


class Command(BaseCommand):
    def handle(self, *args, **options):
        Day.increment()

