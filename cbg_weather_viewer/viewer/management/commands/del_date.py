from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from cbg_weather_viewer.viewer.models import Day
from datetime import datetime, timedelta, time
import re


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--start', action='store')
        parser.add_argument('-e', '--end', action='store')
        parser.add_argument('-d', '--date', action='store')

    def delete_data(self, start_date, delta=0):
        months = []
        years = []
        for i in range(delta + 1):
            day_date = (start_date + timedelta(days=i)).date()
            try:
                d = Day.objects.get(date=day_date)
            except ObjectDoesNotExist:
                self.stdout.write('Date {} not in the database'.format(day_date))
            else:
                if d.month not in months:
                    months.append(d.month)
                    if d.month.year not in years:
                        years.append(d.month.year)
                d.delete()
                self.stdout.write('Date {} deleted'.format(day_date))

        for m in months:
            if m.day_set.count():
                m.update()
            else:
                self.stdout.write('Month {} deleted'.format(m))
                m.delete()

        for y in years:
            if y.month_set.count():
                y.update()
            else:
                self.stdout.write('Year {} deleted'.format(y))
                y.delete()

    def handle(self, *args, **options):
        has_start = (not options['start'] is None)
        has_end = (not options['end'] is None)
        has_date = (not options['date'] is None)
        date_pattern = r'(\d{1,2})\-(\d{1,2})\-(\d{4})'

        if has_end and has_start:
            match_start = re.match(date_pattern, options['start'])
            match_end = re.match(date_pattern, options['end'])
            if match_start and match_end:
                start_date = datetime(int(match_start.group(3)), int(match_start.group(2)), int(match_start.group(1)), 0, 0, 0)
                end_date = datetime(int(match_end.group(3)), int(match_end.group(2)), int(match_end.group(1)), 0, 0, 0)
                delta = (end_date - start_date).days
                if delta >= 0:
                    self.delete_data(start_date, delta)
            else:
                self.stdout.write('Start or End date badly formatted, please enter dates in dd-mm-yyyy format.')
        elif has_date:
            match = re.match(date_pattern, options['date'])
            if match:
                delete_date = datetime(int(match.group(3)), int(match.group(2)), int(match.group(1)), 0, 0, 0)
                self.delete_data(delete_date, 0)
            else:
                self.stdout.write('Date badly formatted, please enter dates in dd-mm-yyyy format.')
        else:
            self.stdout.write('Incorrect use: either provide start (-s, --start) and end date (-e, --end)'
                              ' or the date to be deleted (-d, --date).')

