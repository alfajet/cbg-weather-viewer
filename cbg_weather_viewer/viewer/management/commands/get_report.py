from django.core.management.base import BaseCommand, CommandError
from cbg_weather_viewer.viewer.models import Day, Month, Year
from datetime import datetime, timedelta, time
import re


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--start', action='store')
        parser.add_argument('-e', '--end', action='store')

    def days_import(self, start_date, delta):
        months = []
        years = []
        for i in range(delta):
            day_date = (start_date + timedelta(days=i)).date()
            try:
                d = Day.create(day_date)
            except Exception as e:
                self.stdout.write('Error {}'.format(e))
            else:
                if d:
                    self.stdout.write('{} imported'.format(d))
                    if d.month not in months:
                        months.append(d.month)
                        if d.month.year not in years:
                            years.append(d.month.year)
                else:
                    self.stdout.write('Impossible to create data for {}'.format(day_date))
        for m in months:
            m.update()
        for y in years:
            y.update()

    def handle(self, *args, **options):
        has_start = (not options['start'] is None)
        has_end = (not options['end'] is None)
        date_pattern = r'(\d{1,2})\-(\d{1,2})\-(\d{4})'

        today = datetime.now()
        if has_end and has_start:
            match_start = re.match(date_pattern, options['start'])
            match_end = re.match(date_pattern, options['end'])
            if match_start and match_end:
                start_date = datetime(int(match_start.group(3)), int(match_start.group(2)), int(match_start.group(1)), 0, 0, 0)
                end_date = datetime(int(match_end.group(3)), int(match_end.group(2)), int(match_end.group(1)), 0, 0, 0)
                delta = (end_date - start_date).days
                if delta >= 0:
                    self.stdout.write('{} day(s) will be created.'.format(delta))
                    self.days_import(start_date,  delta + 1)
        elif has_start:
            self.stdout.write('Importing from {}'.format(options['start']))
            match = re.match(date_pattern, options['start'])
            if match:
                start_date = datetime(int(match.group(3)), int(match.group(2)), int(match.group(1)), 0, 0, 0)
                oldest = Day.objects.filter(date__gte=start_date).latest('-date')
                self.stdout.write('Oldest date in the database is: {}'.format(oldest))
                oldest_date = datetime.combine(oldest.date, time.min)
                delta = (oldest_date - start_date).days
                if delta > 0:
                    self.stdout.write('{} day(s) will be created.'.format(delta))
                    self.days_import(start_date, delta)
        elif has_end:
            match = re.match(date_pattern, options['end'])
            if match:
                end_date = datetime(int(match.group(3)), int(match.group(2)), int(match.group(1)), 0, 0, 0)
                latest = Day.objects.latest('date')
                latest_date = datetime.combine(latest.date, time.min)
                delta = (end_date - latest_date).days
                if delta > 0:
                    self.stdout.write('{} day(s) will be created.'.format(delta))
                    self.days_import(latest_date + timedelta(days=1), delta)
        else:
            latest = Day.objects.latest('date')
            latest_date = datetime.combine(latest.date, time.min)
            delta  = (today - latest_date).days
            if delta > 1:
                self.days_import(latest_date + timedelta(days=1), delta - 1)


