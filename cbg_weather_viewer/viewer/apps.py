from django.apps import AppConfig

app_namespace = 'viewer'  # Used in urls

class ViewerConfig(AppConfig):
    name = 'cbg_weather_viewer.viewer'
