from datetime import date, time, datetime
from collections import Counter

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone

from .utils.cbg_requests import fetch_file
from .utils.txtimport import parse_daily_text
from .utils.stats import get_delta, PeriodType, set_period_series, intensive_prop_stats_period, \
    extensive_prop_stats_period, intensive_prop_stats, extensive_prop_stats

# List of choices for wind directions array
WIND_DIRECTIONS = (
    ('N', 'North'),
    ('NE', 'North-East'),
    ('E', 'East'),
    ('SE', 'South-East'),
    ('S', 'South'),
    ('SW', 'South-West'),
    ('W', 'West'),
    ('NW', 'North-West'),
    ('*', 'Unknown'),
)

INTENSIVE_PROPS = ['temperature', 'humidity', 'dew_point', 'pressure', 'wind_speed', 'max_wind_speed']
EXTENSIVE_PROPS = ['sun_hours', 'rain']

PROP_DEFS = {
    'temperature': {'name': 'Temperature', 'unit': '°C'},
    'humidity': {'name': 'Humidity', 'unit': '%'},
    'dew_point': {'name': 'Dew Point', 'unit': '°C'},
    'pressure': {'name': 'Pressure', 'unit': 'hPa'},
    'wind_speed': {'name': 'Wind Speed', 'unit': 'knots'},
    'max_wind_speed': {'name': 'Max Wind Speed', 'unit': 'knots'},
    'sun_hours': {'name': 'Sunshine', 'unit': 'h'},
    'rain': {'name': 'Rain', 'unit': 'mm'},
}


def set_prop_name(prop, with_unit):
    """
    Construct property names from PROP_DEF definition dictionary
    :param prop: property key
    :type prop: str
    :param with_unit: flag controling if unit should be included
    :type with_unit: bool
    :return: Formated property name
    """
    prop_str = PROP_DEFS[prop]['name']
    if with_unit:
        prop_str = prop_str + ' (' + PROP_DEFS[prop]['unit'] + ')'
    return prop_str

class PropertyStatistics(models.Model):
    is_extensive = models.BooleanField(default=False)
    is_intensive = models.BooleanField(default=False)
    avg = models.FloatField(null=True, blank=True)
    total = models.FloatField(null=True, blank=True)   # Intensive fields only
    min = models.FloatField(null=True, blank=True)    # Extensive fields only
    min_date = models.DateTimeField(null=True, blank=True)  # Extensive fields only
    max = models.FloatField(null=True, blank=True)
    max_date = models.DateTimeField(null=True, blank=True)


class PropertyCurves(models.Model):
    is_extensive = models.BooleanField(default=False)
    is_intensive = models.BooleanField(default=False)

    # Extensive fields
    min = ArrayField(models.FloatField(null=True, blank=True), size=31, null=True, blank=True)
    avg = ArrayField(models.FloatField(null=True, blank=True), size=31, null=True, blank=True)
    max = ArrayField(models.FloatField(null=True, blank=True), size=31, null=True, blank=True)

    # Intensive fields
    total = ArrayField(models.FloatField(null=True, blank=True), size=31, null=True, blank=True)


class Period(models.Model):
    date = models.DateField(unique=True)

    # Statistic fields

    temperature_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    humidity_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    dew_point_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    pressure_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    wind_speed_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    sun_hours_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    rain_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')
    max_wind_speed_stats = models.ForeignKey(PropertyStatistics, null=True, on_delete=models.SET_NULL, related_name='+')

    dominant_wind_dir = models.CharField(choices=WIND_DIRECTIONS, max_length=40, null=True, blank=True)
    dominant_wind_dir_ratio = models.FloatField(null=True, blank=True)

    @property
    def dt(self):
        return datetime.combine(self.date, time.min)

    @property
    def prop_dict(self):
        """Return all statistical values as a dict"""
        props = {}
        for prop in INTENSIVE_PROPS:
            fk = getattr(self, prop + '_stats')
            if fk:
                dict = {
                    'Min': fk.min,
                    'Avg': fk.avg,
                    'Max': fk.max,
                }
            else:
                dict = {
                    'Min': None,
                    'Avg.': None,
                    'Max': None
                }
            props[set_prop_name(prop, True)] = dict
        for prop in EXTENSIVE_PROPS:
            fk = getattr(self, prop + '_stats')
            if fk:
                dict = {
                    'Total': fk.total,
                    'Mean': fk.avg,
                    'Max': fk.max,
                }
            else:
                dict = {
                    'Total': None,
                    'Mean': None,
                    'Max': None
                }
            props[set_prop_name(prop, True)] = dict
        return props

    slug = models.CharField(unique=True, max_length=10, null=True, blank=True)

    SLUG_FORMAT = '%Y-%m-%d'  # Format used for Day, surcharged in Year and Month classes

    def init_fk(self):
        for prop in (INTENSIVE_PROPS + EXTENSIVE_PROPS):
            is_intensive = False
            is_extensive = False
            if prop in INTENSIVE_PROPS:
                is_intensive = True
            if prop in EXTENSIVE_PROPS:
                is_extensive = True
            obj = PropertyStatistics.objects.create(is_intensive=is_intensive, is_extensive=is_extensive)
            obj.save()
            setattr(self, prop + '_stats', obj)
            self.save()

    def get_stats(self):
        stats_dict = {}
        for prop in (INTENSIVE_PROPS + EXTENSIVE_PROPS):
            fk = getattr(self, prop + '_stats')
            if fk:
                stats_dict[prop] = fk
        return stats_dict

    def delete(self, *args, **kwargs):
        stats_dict = self.get_stats()
        super().delete(*args, **kwargs)
        for k, v in stats_dict.items():
            if v:
                v.delete()

    def _set_slug(self):
        self.slug = self.date.strftime(self.SLUG_FORMAT)

    def save(self, *args, **kwargs):
        if self.slug is None:
            self._set_slug()
        super().save(*args, **kwargs)

    def __str__(self):
        if self.slug is None:
            self._set_slug()
        return self.slug

    def __repr__(self):
        return f'{self.__class__.__name__}({self.__str__()})'

    class Meta:
        abstract = True
        # ordering = ['-date']


class PeriodSeries(Period):
    # New PropertyCurves
    temperature_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    humidity_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    dew_point_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    pressure_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    wind_speed_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    sun_hours_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    rain_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')
    max_wind_speed_series = models.ForeignKey(PropertyCurves, null=True, on_delete=models.SET_NULL, related_name='+')

    rain_days_without = models.IntegerField(null=True)

    def init_fk(self):
        for prop in (INTENSIVE_PROPS + EXTENSIVE_PROPS):
            is_intensive = False
            is_extensive = False
            if prop in INTENSIVE_PROPS:
                is_intensive = True
            if prop in EXTENSIVE_PROPS:
                is_extensive = True
            obj = PropertyCurves.objects.create(is_intensive=is_intensive, is_extensive=is_extensive)
            obj.save()
            setattr(self, prop + '_series', obj)
            self.save()
        super().init_fk()

    def get_series(self):
        series_dict = {}
        for prop in (INTENSIVE_PROPS + EXTENSIVE_PROPS):
            fk = getattr(self, prop + '_series')
            if fk:
                series_dict[prop] = fk
        return series_dict

    def set_series(self):
        pass

    def calc_stats(self):
        pass

    def update(self):
        self.set_series()
        self.calc_stats()
        self.save()

    def delete(self, *args, **kwargs):
        series_dict = self.get_series()
        super().delete(*args, **kwargs)
        for k, v in series_dict.items():
            if v:
                v.delete()

    @classmethod
    def refresh(cls):
        for obj in cls.objects.all():
            obj.update()

    class Meta:
        abstract = True


class Year(PeriodSeries):
    SLUG_FORMAT = '%Y'

    def __str__(self):
        return self.date.strftime('%Y')

    def set_series(self):
        set_period_series(self, PeriodType.YEAR, INTENSIVE_PROPS, ['min', 'max', 'avg'])
        set_period_series(self, PeriodType.YEAR, EXTENSIVE_PROPS, ['total'])

    def calc_stats(self):
        intensive_prop_stats_period(self, INTENSIVE_PROPS, ['min', 'max', 'avg'])
        extensive_prop_stats_period(self, EXTENSIVE_PROPS)
        self.rain_days_without = self.month_set.aggregate(sum=models.Sum('rain_days_without'))['sum']

    class Meta:
        ordering = ['-date']


class Month(PeriodSeries):
    SLUG_FORMAT = '%Y-%m'

    year = models.ForeignKey(Year, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.date.strftime('%B %Y')

    def set_series(self):
        set_period_series(self, PeriodType.MONTH, INTENSIVE_PROPS, ['min', 'max', 'avg'])
        set_period_series(self, PeriodType.MONTH, EXTENSIVE_PROPS, ['total'])

    def calc_stats(self):
        intensive_prop_stats_period(self, INTENSIVE_PROPS, ['min', 'max', 'avg'])
        extensive_prop_stats_period(self, EXTENSIVE_PROPS)

        self.rain_days_without = self.day_set.filter(rain_stats__total=0).count()

    class Meta:
        ordering = ['date']


class Day(Period):
    MISSING_DATA_TEXT = 'Date unknown.'
    month = models.ForeignKey(Month, null=False, on_delete=models.CASCADE)

    # Raw data
    time = ArrayField(models.TimeField(null=True, blank=True), size=49, null=True, blank=True)
    temperature = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    humidity = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    dew_point = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    pressure = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    wind_speed = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    wind_dir = ArrayField(models.CharField(choices=WIND_DIRECTIONS, max_length=40, null=True, blank=True), size=49, null=True, blank=True)
    sun_hours = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    sun_hours_delta = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    rain = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    rain_delta = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)
    max_wind_speed = ArrayField(models.FloatField(null=True, blank=True), size=49, null=True, blank=True)

    @property
    def sun_hours_percent(self):
        if not self.sun_hours_delta:
            return None
        else:
            return [x * 200 for x in self.sun_hours_delta]


    @property
    def has_data(self):
        if not self.time:
            return False
        else:
            return len(self.time) > 0

    def __str__(self):
        return self.date.strftime('%A %x')

    def get_data(self, data=None):
        if not data:
            data = fetch_file('day', self.date.strftime('%Y-%m-%d'))
        property_dict = {
            0: 'time',
            1: 'temperature',
            2: 'humidity',
            3: 'dew_point',
            4: 'pressure',
            5: 'wind_speed',
            6: 'wind_dir',
            7: 'sun_hours',
            8: 'rain',
            9: 'max_wind_speed'
        }

        if data == self.MISSING_DATA_TEXT:
            return False
        else:
            properties = parse_daily_text(data)
            if len(properties[0]) > 0:
                for k, v in property_dict.items():
                    if len(properties[k]) > 0:
                        setattr(self, v, properties[k])

                self.sun_hours_delta = get_delta(self.sun_hours)
                self.rain_delta = get_delta(self.rain)
                return True
            else:
                return False

    def calc_stats(self):

        for prop in INTENSIVE_PROPS:
            stats = getattr(self, prop + '_stats')
            intensive_prop_stats(self, prop, stats)
            stats.save()

        stats = getattr(self, 'sun_hours_stats')
        extensive_prop_stats(self, 'sun_hours', stats)

        stats = getattr(self, 'rain_stats')
        extensive_prop_stats(self, 'rain', stats)

        if self.wind_dir:
            dominant_wind = Counter(self.wind_dir).most_common(1)
            self.dominant_wind_dir = dominant_wind[0][0]
            self.dominant_wind_dir_ratio = dominant_wind[0][1] / len(self.wind_dir)

    @classmethod
    def create(cls, day, refresh=False):
        """
        :param day: date to be created
        :type day: date
        :param refresh: if true, month and year ancestors statistics are updated.
        :return:
        """

        data = fetch_file('day', day.strftime('%Y-%m-%d'))
        if data == cls.MISSING_DATA_TEXT:
            return None
        else:
            year, created = Year.objects.get_or_create(date=date(day.year, 1, 1))
            if created:
                year.init_fk()
                year.save()
            month, created = Month.objects.get_or_create(date=date(day.year, day.month, 1), defaults={'year': year})
            if created:
                month.init_fk()
                month.save()
            day_obj, day_created = cls.objects.get_or_create(date=day, defaults={'month': month})
            if day_created:
                day_obj.init_fk()
                day_obj.save()

            if day_created:
                day_obj.get_data()
                if day_obj.has_data:
                    day_obj.calc_stats()
                    if refresh:
                        month.update()
                        year.update()
            day_obj.save()

            return day_obj

    @classmethod
    def refresh(cls):
        for d in cls.objects.all():
            d.calc_stats()
            d.save()
        Month.refresh()
        Year.refresh()


    @classmethod
    def increment(cls):
        last_day = cls.objects.latest('date')
        now = datetime.now()

        delta = (now - datetime.combine(last_day.date, time.min)).days
        if delta > 1:
            # todo: raise exception
            return

        last_day.get_data()
        if last_day.has_data:
            last_day.calc_stats()
        last_day.save()

        if now.date != last_day.date:
            last_day.month.update()
            last_day.month.year.update()
            cls.create(now.date(), refresh=False)

    class Meta:
        ordering = ['-date']
