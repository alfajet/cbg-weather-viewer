from io import StringIO
from datetime import time
from time import strptime

ESC_CHAR = '#'


def parse_time(foo):
    try:
        time_struct = strptime(foo, '%H:%M')  # https://docs.python.org/3/library/time.html#time.struct_time
        t = time(time_struct[3], time_struct[4])
        return t
    except ValueError:
        return None


def parse_float(foo):
    try:
        return float(foo)
    except ValueError:
        return None


def parse_wind_dir(foo):
    if foo == '*':
        return None
    else:
        return foo


def parse_daily_text(text):
    stream = StringIO(text)
    day_time = []
    temp = []
    humid = []
    dew = []
    press = []
    windsp = []
    winddir = []
    sun = []
    rain = []
    mxwind = []

    while True:
        line = stream.readline()
        if not line:
            break
        if not line[0] == ESC_CHAR:
            split = line.split('\t')
            day_time.append(parse_time(split[0]))
            temp.append(parse_float(split[1]))
            humid.append(parse_float(split[2]))
            dew.append(parse_float(split[3]))
            press.append(parse_float(split[4]))
            windsp.append(parse_float(split[5]))
            winddir.append(parse_wind_dir(split[6]))
            sun.append(parse_float(split[7]))
            rain.append(parse_float(split[8]))
            # 'Start' Column not imported
            mxwind.append(parse_float(split[10]))

    return day_time, temp, humid, dew, press, windsp, winddir, sun, rain, mxwind
