from datetime import date, datetime
import pytz

from enum import Enum
from calendar import monthrange
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

class PeriodType(Enum):
    MONTH = 0
    YEAR = 1


def intensive_prop_stats(day, prop, stats):
    """
    Populate results in stats object
    """
    src = getattr(day, prop)
    if src:
        filtered_prop = list(filter(None.__ne__, src))  # Excluding null values from the list
        stats.min = min(filtered_prop)
        # stats.min_date = datetime.combine(day.date, day.time[src.index(stats.min)], tzinfo=pytz.utc)
        # Removed timezone parameter for backward compatibility with Python 3.5
        stats.min_date = timezone.make_aware(datetime.combine(day.date, day.time[src.index(stats.min)]))

        stats.max = max(filtered_prop)
        # stats.max_time = datetime.combine(day.date, day.time[src.index(stats.max)], tzinfo=pytz.utc)
        stats.max_time = timezone.make_aware(datetime.combine(day.date, day.time[src.index(stats.max)]))

        stats.avg = sum(filtered_prop) / float(len(filtered_prop))
        stats.save()


def extensive_prop_stats(day, prop, stats):
    """
    Populate results in stats object
    """
    src = getattr(day, prop)
    if src:
        filtered_prop = list(filter(None.__ne__, src))  # Excluding null values from the list
        stats.total = max(filtered_prop)
        stats.save()


def get_delta(cumulative):
    delta = []

    for i, v in enumerate(cumulative):
        if i == 0:
            delta.append(v)
        else:
            delta.append(v - cumulative[i - 1])
    return delta


def set_period_series(period, period_type, props, prop_types):
    if period_type == PeriodType.MONTH:
        sources = period.day_set.all()
        rng = range(1, monthrange(period.date.year, period.date.month)[1] + 1)
    elif period_type == PeriodType.YEAR:
        sources = period.month_set.all()
        rng = range(1, 13)

    series_dict = period.get_series()
    # Reinitialise the series
    for p in props:
        series_obj = series_dict[p]
        for t in prop_types:
            setattr(series_obj, t, [])

    for i in rng:
        try:
            if period_type == PeriodType.MONTH:
                src = sources.get(date=date(period.date.year, period.date.month, i))
            else:
                src = sources.get(date=date(period.date.year, i, 1))
        except ObjectDoesNotExist:
            for p in props:
                for t in prop_types:
                    series = getattr(series_dict[p], t)
                    if series is None:
                        setattr(series_dict[p], t, [None])
                    else:
                        series.append(None)
        else:
            stats_dict = src.get_stats()
            for p in props:
                stats_obj = stats_dict[p]
                series_obj = series_dict[p]
                
                for t in prop_types:
                    series = getattr(series_obj, t)
                    value = getattr(stats_obj, t)
                    if series is None:
                        try:
                            setattr(series_obj, t, [float(value)])
                        except TypeError:
                            setattr(series_obj, t, [None])
                    else:
                        try:
                            series.append(float(value))
                        except TypeError:
                            series.append(None)
                series_obj.save()


def intensive_prop_stats_period(period, props, series_mapping):
    # calculating avg min, avg & max of the current period property curves
    series_obj = period.get_series()
    stats_obj = period.get_stats()
    for prop in props:
        for t in series_mapping:
            src = getattr(series_obj[prop], t)
            # print('src: {}'.format(src))
            filtered_src = list(filter(None.__ne__, src))
            avg = sum(filtered_src) / float(len(filtered_src))
            setattr(stats_obj[prop], t, avg)
            stats_obj[prop].save()


def extensive_prop_stats_period(period, props):
    # calculating avg min, avg & max of the current period property curves
    series_obj = period.get_series()
    stats_obj = period.get_stats()
    for prop in props:
        src = getattr(series_obj[prop], 'total')
        filtered_src = list(filter(None.__ne__, src))
        setattr(stats_obj[prop], 'avg', sum(filtered_src) / float(len(filtered_src)))
        setattr(stats_obj[prop], 'total', sum(filtered_src))
        setattr(stats_obj[prop], 'max', max(filtered_src))
        stats_obj[prop].save()

def aggregate(period, intensive_props, extensive_props):
    """
    :param period: queryset of Day, Month or Year instances
    :return: data dictionary aggregating the object statistical values in a dictionary
    """
    data = {}
    period_type = period.model.__name__
    data['period_type'] = period_type

    periods = []
    for _ in period:
        periods.append(_.slug)
    periods_name = period_type.lower() + 's'
    data[periods_name] = periods

    values = {}
    for prop in intensive_props:
        min = []
        avg = []
        max = []
        for item in period:
            item_prop = getattr(item, prop + '_stats')
            min.append(item_prop.min)
            avg.append(item_prop.avg)
            max.append(item_prop.max)
        values[prop] = {
            'id': prop,
            'caption': prop.capitalize().replace('_', ' '),
            'is_intensive': True,
            'is_extensive': False,
            'min': min,
            'avg': avg,
            'max': max,
        }

    for prop in extensive_props:
        total = []
        for item in period:
            item_prop = getattr(item, prop + '_stats')
            total.append(item_prop.total)
        values[prop] = {
            'id': prop,
            'caption': prop.capitalize().replace('_', ' '),
            'is_intensive': False,
            'is_extensive': True,
            'total': total,
        }
    data['props'] = values

    return data



