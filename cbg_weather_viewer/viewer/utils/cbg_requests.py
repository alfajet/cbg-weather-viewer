# -*- coding: utf-8 -*-

"""Main module."""
import re
import requests
import datetime

BASE_URL = "http://www.cl.cam.ac.uk/research/dtg/weather/"
DAILY = "daily-text.cgi"
PERIOD = "period-text.cgi"


YEAR = r'(\d{4})'
MONTH = r'(\d{4})-(\d{2})'
DAY = r'(\d{4})-(\d{2})-(\d{2})'


# def get_periods_data(period_type, range):
#     periods = generate_periods(period_type, range)
#     for p in periods:
#         data = fetch_file(period_type, p)



def set_periods(period_type, range):
    """
    from a min, max value, get a collection of periods to be generated
    :param period_type:
    :param range:
    :return:
    """
    date_formats = {
        'day': '%Y-%m-%d',
        'month': '%Y-%m',
        'year': '%Y',
    }

    start, end = [datetime.datetime.strptime(_, date_formats[period_type]) for _ in range.split()]
    if end < start:
        raise Exception # end can't be before start

    periods = [start]
    increment = increment_period(start, period_type)

    while increment < end:
        periods.append(increment)
        increment = increment_period(increment, period_type)

    periods.append(end)

    return periods


def increment_period(date, period_type):
    """

    :param date: original date
    :type date: datetime.datetime
    :param period_type: type of increment, can be day, month or year
    :return: incremented date, by either 1 day, 1 month or 1 year
    """
    if period_type == 'day':
        return date + datetime.timedelta(days=1)
    elif period_type == 'month':
        month = (date.month % 12) + 1
        year = date.year + date.month // 12
        return datetime.datetime(year=year, month=month, day=1)
    elif period_type == 'year':
        return datetime.datetime(year=date.year+1, month=1, day=1)
    else:
        # Todo: add better exception
        raise Exception


def fetch_file(period_type, period):
    """
    Main function, taking as argument a period type (day, month, year) and a specific period.
    It returns a file
    :param period_type:
    :param period:
    :return:
    """

    url = get_url(period_type, period)
    r = requests.get(url)
    r.raise_for_status()

    return r.text


def get_url(period_type, period):
    re_test = {
        'year': YEAR,
        'month': MONTH,
        'day': DAY,
    }

    if not re.match(re_test[period_type], period):
        raise Exception

    if period_type == 'day':
        page = DAILY
    else:
        page = PERIOD

    url = BASE_URL + page + '?' + period
    return url
