from collections import OrderedDict
from django.db import models
from rest_framework import serializers
from .models import Day, Month, Year, PropertyStatistics, PropertyCurves, INTENSIVE_PROPS, EXTENSIVE_PROPS


# from .apps import app_namespace

# def get_view_name(view_name):
#     return f'api:{view_name}
#     return f'{app_namespace}:{view_name}'


def get_stat_fields():
    return [field + '_stats' for field in INTENSIVE_PROPS + EXTENSIVE_PROPS]


def promote_fields(model: models.Model, *fields):
    promoted_fields = list(fields)
    excluded_fields = promoted_fields +  ['id', 'date']
    other_fields = [field.name for field in model._meta.fields if field.name not in excluded_fields]
    return promoted_fields + other_fields


# class PropertyStatisticsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = PropertyStatistics
#         fields = '__all__'


class PropertyIntensiveStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyStatistics
        fields = ['avg', 'min', 'max']


class PropertyExtensiveStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyStatistics
        fields = ['avg', 'total', 'max']


class PropertyCurvesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyCurves
        fields = '__all__'


class PropertyIntensiveCurvesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyCurves
        fields = ['avg', 'min', 'max']


class PropertyExtensiveCurvesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyCurves
        fields = ['avg', 'total', 'max']


class StatsBase(object):
    intensive_fields = [(prop + '_stats', PropertyIntensiveStatisticsSerializer(read_only=True, many=False))
                        for prop in INTENSIVE_PROPS]
    extensive_fields = [(prop + '_stats', PropertyExtensiveStatisticsSerializer(read_only=True, many=False))
                        for prop in EXTENSIVE_PROPS]
    _declared_fields = OrderedDict(intensive_fields + extensive_fields)


class CurveBase(object):
    intensive_fields = [(prop + '_series', PropertyIntensiveCurvesSerializer(read_only=True, many=False))
                        for prop in INTENSIVE_PROPS]
    extensive_fields = [(prop + '_series', PropertyExtensiveCurvesSerializer(read_only=True, many=False))
                        for prop in EXTENSIVE_PROPS]
    _declared_fields = OrderedDict(intensive_fields + extensive_fields)


class CurvesStatsBase(object):
    _declared_fields = OrderedDict(list(CurveBase._declared_fields.items())
                                   + list(StatsBase._declared_fields.items()))


    # intensive_fields = OrderedDict([(k, v) for k, v in StatsBase._declared_fields.items()] +
    #                                [(prop + '_series', PropertyCurvesSerializer(read_only=True, many=False))
    #                                 for prop in EXTENSIVE_PROPS + INTENSIVE_PROPS])


class YearDetailSerializer(CurvesStatsBase, serializers.ModelSerializer):
    month_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        # view_name=  get_view_name('api_month-detail'),
        view_name='month-detail',
        lookup_field='slug'
    )

    class Meta:
        model = Year
        fields = promote_fields(model, 'slug', 'month_set')


# class YearMonthsSerializer(serializers.HyperlinkedModelSerializer):
#     month_set = serializers.HyperlinkedRelatedField(
#         many=True,
#         read_only=True,
#         # view_name=  get_view_name('api_month-detail'),
#         view_name=  'month-detail',
#         lookup_field='slug'
#     )
#
#     class Meta:
#         model = Year
#         fields = ['month_set']

class YearSummarySerializer(StatsBase, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Year
        fields = ['url'] + get_stat_fields()
        extra_kwargs = {'url': {'lookup_field': 'slug'}}


class MonthDetailSerializer(CurvesStatsBase, serializers.ModelSerializer):
    year = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='year-detail',
        lookup_field='slug'
    )

    day_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='day-detail',
        # view_name=get_view_name('api_day-detail'),
        lookup_field='slug'
    )

    class Meta:
        model = Month
        fields = promote_fields(model, 'slug', 'year', 'day_set')


class MonthSummarySerializer(StatsBase, serializers.HyperlinkedModelSerializer):
    # month = serializers.HyperlinkedIdentityField(get_view_name('month-detail'), lookup_field='slug')
    class Meta:
        model = Month
        fields = ['url'] + get_stat_fields()
        extra_kwargs = {'url': {'lookup_field': 'slug'}}


class DayDetailSerializer(StatsBase, serializers.ModelSerializer):
    month = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='month-detail',
        lookup_field='slug'
    )

    class Meta:
        model = Day
        fields = promote_fields(model, 'slug', 'month')


class DaySummarySerializer(StatsBase, serializers.HyperlinkedModelSerializer):
    # day = serializers.HyperlinkedIdentityField('day-detail', lookup_field='slug')

    class Meta:
        model = Day
        fields = ['url'] + get_stat_fields()
        extra_kwargs = {'url': {'lookup_field': 'slug'}}
