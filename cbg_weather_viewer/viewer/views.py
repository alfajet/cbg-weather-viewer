from collections import OrderedDict
#from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from django.http import JsonResponse
import json
from cbg_weather_viewer.viewer.models import Year, Month, Day, INTENSIVE_PROPS, EXTENSIVE_PROPS
from .utils.stats import aggregate


class YearList(ListView):
    model = Year
    queryset = Year.objects.all().order_by('-date')
    context_object_name = 'years'


class YearSummary(ListView):
    model = Year
    queryset = Year.objects.all().order_by('-date')
    context_object_name = 'years'
    template_name = 'viewer/year_summary.html'


class YearDetails(DetailView):
    model = Year
    context_object_name = 'year'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['nb_months'] = range(1, 13)
        return context


class MonthDetails(DetailView):
    model = Month
    context_object_name = 'month'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['nb_days'] = range(1, 32)
        return context

class DayRange(ListView):
    model = Day
    context_object_name = 'days'
    template_name = 'viewer/day_range.html'

    def get_queryset(self):
        queryset = Day.objects.filter(date__range=[self.kwargs['start'], self.kwargs['end']])
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['start'] = self.kwargs['start']
        context['end'] = self.kwargs['end']
        context['day_rng'] = [x.date.day for x in self.object_list]
        context['data'] = aggregate(self.object_list, INTENSIVE_PROPS, EXTENSIVE_PROPS)
        # self.day_rng = context['day_rng']

        return context

class DayDetails(DetailView):
    model = Day
    context_object_name = 'day'
    slug_field = 'slug'

    def get_object(self):
        if 'slug' not in self.kwargs:
            return Day.objects.first()
        return super(DayDetails, self).get_object()


class CompareView(TemplateView):
    template_name = 'viewer/compare.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['years'] = [y.date.year for y in Year.objects.all().order_by('date')]
        context['months'] = [m.date.strftime('%Y-%m') for m in Month.objects.all().order_by('date')]
        return context


def ajax_compare(request):
    period_type = request.GET.get('period_type')
    response = None
    if period_type == 'year':
        years = [int(x) for x in request.GET.get('periods').split(",")]
        year_obj = Year.objects.filter(date__year__in=years).order_by('date')
        curve_fk = request.GET.get('curve') + '_series'
        y_values = OrderedDict()
        for year in year_obj:
            curve_obj = getattr(year, curve_fk)
            series = getattr(curve_obj, request.GET.get('curve_type'))
            y_values[str(year)] = [round(x, 2) for x in series]
        response = {
            'x_values': ",".join(map(str, range(1, 13))),
            'y_values': y_values
        }
    elif period_type == 'month':
        months = [x.strip() + '-01' for x in request.GET.get('periods').split(",")]
        month_obj = Month.objects.filter(date__in=months).order_by('date')
        curve_fk = request.GET.get('curve') + '_series'
        y_values = OrderedDict()
        for month in month_obj:
            curve_obj = getattr(month, curve_fk)
            series = getattr(curve_obj, request.GET.get('curve_type'))
            y_values[str(month)] = [round(x, 2) for x in series]
        response = {
            'x_values': ",".join(map(str, range(1, 32))),
            'y_values': y_values
        }
    elif period_type == 'day':
        days = [x.strip() for x in request.GET.get('periods').split(",")]
        day_obj = Day.objects.filter(date__in=days).order_by('date')
        curve_fk = request.GET.get('curve')
        y_values = OrderedDict()
        for day in day_obj:
            curve_obj = getattr(day, curve_fk)
            y_values[str(day)] = [round(x, 2) for x in curve_obj]
        response = {
            'x_values': ",".join([t.strftime('%H:%M') for t in day_obj[0].time]),
            'y_values': y_values
        }
    else:
        response = False
    return JsonResponse(response)
