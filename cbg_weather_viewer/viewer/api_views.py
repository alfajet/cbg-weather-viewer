from .models import Year, Month, Day
from .serializers import YearDetailSerializer, YearSummarySerializer, \
    MonthDetailSerializer, MonthSummarySerializer, \
    DayDetailSerializer, DaySummarySerializer, \
    INTENSIVE_PROPS, EXTENSIVE_PROPS
# from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action
from django_filters import rest_framework as filters
from collections import namedtuple


class FiltersMixin():
    def define_filters(self, model):
        def set_stats_vars(prop, stat):
            name_min = f'{stat}_{prop}_min'
            name_max = f'{stat}_{prop}_max'
            filter_min = filters.NumberFilter(field_name=f'{prop}_stats__{stat}', lookup_expr='gte')
            filter_max = filters.NumberFilter(field_name=f'{prop}_stats__{stat}', lookup_expr='lte')
            return [(name_min, filter_min), (name_max, filter_max)]

        filter_list = []

        # Set Date filters
        filter_list.append(('date_year', filters.NumberFilter(field_name='date', lookup_expr='year')))
        if model.__name__ != 'Year': # i.e. applies to Day and Month models
            filter_list.append(('date_month', filters.NumberFilter(field_name='date', lookup_expr='month')))
        if model.__name__ == 'Day':
            filter_list.append(('date_day', filters.NumberFilter(field_name='date', lookup_expr='day')))

        #Set properties filters
        PropDef = namedtuple('PropDef', ['property', 'stats'])
        props = []
        for p in INTENSIVE_PROPS:
            props.append(PropDef(property=p, stats=['avg','min','max']))

        extensive_stats = ['total']
        if model.__name__ != 'Day':
            extensive_stats.extend(['avg', 'total'])

        for p in EXTENSIVE_PROPS:
            props.append(PropDef(property=p, stats=extensive_stats))

        for p in props:
            for stat in p.stats:
                filter_list.extend(set_stats_vars(p.property, stat))

        filter_names = [name for name, filter in filter_list]
        Meta = type('Meta', (object,), {'model': model, 'fields': filter_names})

        cls_attributes = dict(filter_list)
        cls_attributes['Meta'] = Meta

        filterset_class = type('StatsFilterSets', (filters.FilterSet, ), cls_attributes)

        return filterset_class


class YearViewSet(FiltersMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Year.objects.all().order_by('-date')
    serializer_class = YearSummarySerializer
    lookup_field = 'slug'
    filter_backends = [filters.DjangoFilterBackend]

    def __init__(self, **kwargs):
        self.filterset_class = self.define_filters(Year)
        super().__init__(**kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = YearDetailSerializer(instance=instance, context={'request': request})
        return Response(serializer.data)

    # @action(detail=True)
    # def months(self, request, *args, **kwargs):
    #     serializer = YearMonthsSerializer(instance=self.get_object(), context={'request': request})
    #     return Response(serializer.data)

class MonthViewSet(FiltersMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Month.objects.all().order_by('-date')
    serializer_class = MonthSummarySerializer
    lookup_field = 'slug'
    filter_backends = [filters.DjangoFilterBackend]

    def __init__(self, **kwargs):
        self.filterset_class = self.define_filters(Month)
        super().__init__(**kwargs)

    # def get_queryset(self):
    #     queryset = Month.objects.all().order_by('-date')
    #     month = self.request.query_params.get('month', None)
    #     if month is not None:
    #         try:
    #             if 1 <= int(month) <= 12:
    #                 queryset =  queryset.filter(date__month=month)
    #         except ValueError:
    #             pass
    #     return queryset

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = MonthDetailSerializer(instance=instance, context={'request': request})
        return Response(serializer.data)

#
# class DayFilter(filters.FilterSet):
#     avg_temperature_min = filters.NumberFilter(field_name='temperature_stats__avg', lookup_expr='gte')
#     avg_temperature_max = filters.NumberFilter(field_name='temperature_stats__avg', lookup_expr='lte')
#
#     class Meta:
#         model = Day
#         fields = ['avg_temperature_min', 'avg_temperature_max']


# class DateFilter(filters.FilterSet):
#     date_month = filters.NumberFilter(field_name='date__month', lookup_expr='exact')
#     date_year = filters.NumberFilter(field_name='date', lookup_expr='year')
#
#     class Meta:
#         model = Day
#         fields = ['date_month', 'date_year']
#
class DayViewSet(FiltersMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
# class DayViewSet( mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Day.objects.all().order_by('-date')
    serializer_class = DaySummarySerializer
    lookup_field = 'slug'
    filter_backends = [filters.DjangoFilterBackend]
    # filterset_class = DateFilter

    def __init__(self, **kwargs):
        self.filterset_class = self.define_filters(Day)
        super().__init__(**kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = DayDetailSerializer(instance=instance, context={'request': request})
        return Response(serializer.data)

