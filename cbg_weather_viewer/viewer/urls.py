import datetime
from django.urls import path, re_path, register_converter

from . import views
from .apps import app_namespace

app_name = app_namespace

class IsoDateConverter:
    regex = r'\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        return datetime.datetime.strptime(value, '%Y-%m-%d').date()

    def to_url(self, value):
        return str(value)

register_converter(IsoDateConverter, 'isodate')


urlpatterns = [
    path('', views.YearList.as_view(), name='home'),
    path('years', views.YearSummary.as_view(), name='years'),
    re_path(r'^(?P<slug>\d{4})/$', views.YearDetails.as_view(), name='year'),
    re_path(r'^(?P<slug>\d{4}\-\d{2}\-\d{2})/$', views.DayDetails.as_view(), name='day'),
    re_path(r'^(?P<slug>\d{4}\-\d{2})/$', views.MonthDetails.as_view(), name='month'),
    # re_path(r'^(?P<slug>\d{4}-\d{2})/$', YearDetails.as_view(), name='month')
    path('compare/', views.CompareView.as_view(), name='compare'),
    path('latest/', views.DayDetails.as_view(), name='latest'),
    path('range/<isodate:start>/<isodate:end>/', views.DayRange.as_view(), name='range'),
    path("ajax/compare", view=views.ajax_compare, name="ajax_compare"),
]
