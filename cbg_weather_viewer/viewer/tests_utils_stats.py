from django.test import TestCase
from .utils.stats import intensive_prop_stats, extensive_prop_stats
from .models import Day
# from django.core.management import call_command


class TestStatisitcs(TestCase):

    fixtures = ['viewer', ]

    def setUp(self):
        self.intensive_test_date = Day.objects.get(date='2019-01-01')
        self.intensive_test_date.temperature_stats.delete()
        self.extensive_test_date = Day.objects.get(date='2019-01-08')
        self.extensive_test_date.sun_hours_stats.delete()

    def test_intensive_stats(self):
        intensive_prop_stats(self.intensive_test_date, 'temperature', self.intensive_test_date.temperature_stats)
        self.assertAlmostEqual(self.intensive_test_date.temperature_stats.min, 1.6)
        self.assertAlmostEqual(self.intensive_test_date.temperature_stats.max, 8.8)
        self.assertAlmostEqual(self.intensive_test_date.temperature_stats.avg, 5.67, delta=0.01)

    def test_extensive_stats(self):
        extensive_prop_stats(self.extensive_test_date, 'sun_hours', self.extensive_test_date.sun_hours_stats)
        self.assertAlmostEqual(self.extensive_test_date.sun_hours_stats.total, 5.6, delta=0.1)
