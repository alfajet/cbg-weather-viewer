from django.urls import path, include
from . import api_views
from rest_framework.routers import DefaultRouter

# app_name = 'api'

api_router = DefaultRouter()

api_router.register('year', api_views.YearViewSet)
# api_router.register('year', api_views.YearViewSet, 'api_year')
api_router.register('month', api_views.MonthViewSet, basename='month')
# api_router.register('month', api_views.MonthViewSet, 'api_month')
api_router.register('day', api_views.DayViewSet)
# api_router.register('day', api_views.DayViewSet, 'api_day')
# api_router.register('month_compare', api_views.MonthCompareViewSet, 'api_mpnth_compare')

urlpatterns = [
    path('', include(api_router.urls)),
]
